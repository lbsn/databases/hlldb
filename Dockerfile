FROM postgres:13.8

# https://manpages.debian.org/stretch/debconf-doc/debconf.7.en.html#Frontends
ARG DEBIAN_FRONTEND=noninteractive

# Postgis
# https://github.com/appropriate/docker-postgis
RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        postgresql-13-postgis-3 \
        postgresql-13-postgis-3-scripts \
        postgis \
    && rm -rf /var/lib/apt/lists/*

# Citus Plugins
# due to general paranoia, do not curl | bash, but use local copy
# https://github.com/citusdata/docker/blob/ff2ff4e2/Dockerfile#L19
COPY get-citusdata-packages.sh .
# https://github.com/citusdata/docker
RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        ca-certificates \
    && bash get-citusdata-packages.sh && rm get-citusdata-packages.sh \
    && apt-get install -y postgresql-$PG_MAJOR-hll=2.17.citus-1 \
                          postgresql-$PG_MAJOR-topn=2.5.0.citus-1 \
    && rm -rf /var/lib/apt/lists/*
