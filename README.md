[![version](https://lbsn.vgiscience.org/databases/hlldb/version.svg)](https://gitlab.vgiscience.de/lbsn/databases/hlldb/tree/master) [![pipeline status](https://gitlab.vgiscience.de/lbsn/databases/hlldb/badges/master/pipeline.svg)](https://gitlab.vgiscience.de/lbsn/databases/hlldb/commits/master)

# LBSN HLL Database - Docker Container

This is a preconfigured docker container based on the HLL version of the [common location based social network (LBSN) data structure concept](https://lbsn.vgiscience.org) to handle cross network Social Media data.
This docker container can be locally started to use the hll version of the LBSN Structure, e.g. for visual analytics.

A complete guide is provided in the [LBSN documentation](https://lbsn.vgiscience.org)

**tl:dr**

To Start the docker container locally:

```bash
git clone --recursive git@gitlab.vgiscience.de:lbsn/databases/hlldb.git
cd hlldb
mv .env.example .env
# optionally: adjust parameters in .env
docker network create lbsn-network
docker-compose up -d
```

## Startup options

The postgres data can be created from scratch, initializing an empty lbsn structure. It is also possible to start the container using a backup volume (e.g. that was archived as a tar).

The two commands are:

* init: 
```
docker-compose -f docker-compose.yml -f docker-compose.init.yml up -d
```

This is also what happens by default, i.e. `docker-compose up -d` will init, if no volume is given. 

* import: 
```
docker-compose -f docker-compose.yml -f docker-compose.import.yml up -d
```

## Local development

To start the docker container use:

```bash
docker-compose up -d
```

To dump and recreate the database container and watch processing the init scripts:

```bash
docker-compose down && docker-compose up --build --detach && docker-compose logs --follow
```

