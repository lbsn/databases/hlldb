/* Perform HLL intersection
 * see the Inclusion-exclusion principle [^1]
 *
 * [1]: en.wikipedia.org/wiki/Inclusion%E2%80%93exclusion_principle
 */
CREATE OR REPLACE FUNCTION 
    extensions.hll_intersection (hll_a hll, hll_b hll)
RETURNS int
AS $$
    SELECT "is".intersection_count FROM
        (SELECT 
            hll_cardinality(hll_a)::int + 
            hll_cardinality(hll_b)::int - 
            hll_cardinality(hll_union(hll_a,hll_b))::int as intersection_count) as "is"
$$
LANGUAGE SQL
STRICT;

