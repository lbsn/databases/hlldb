#!/usr/bin/env bash

# source main init script to import notice function
source /docker-entrypoint-initdb.d/init.sh --no-exec

# start processing files in structure submodule
for file in /structure/*.sql; do
    notice "running SQL from $file on database \`$DATABASE_NAME\`"
    cat "$file" | psql -v ON_ERROR_STOP=1 --dbname "$DATABASE_NAME"
done
